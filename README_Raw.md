# Codebase Standards

---
### Code style
> ### Naming
>Correct properties and methods naming increase the readability of the code.
>You must follow convenience rules [API Design Guidelines](https://swift.org/documentation/api-design-guidelines/). 

> Below you will see the constructions that we use in the whole project and to change some of them you need to create a review meeting with explanations why we need to change it, and if it's okay the change will be approved.

> Always use `Control+I` in XCode to format code.

#### Spacings

Never use single-line syntax for properties.

**Example**:
	
	Wrong:
	var value: String? { return self?.value }
	
	Correct:
	var value: String? { 
        return self?.value
	}

#### Guard

In case of a single-line `guard`, `return` must be moved to the new line.

**Example**:

  	guard let something = some else {
  	    return
	}


In case there are several conditions we must move each one of them on a new line with one tab, also `else` has to be on the same level with `guard`.

**Example**:

  	guard
  	    let something = some,
  	    let something1 = some1
  	else {
  	    return
	}

For weak `self` we use standard naming.

**Example**:

  	guard let self = self else {
  	    return
  	}
	
#### Ternary operator

In case when the conditions and variants of execution are simple, and there is assigning action, we should use the ternary operator.

**Example**:

	bigTerm ? variant : variant1
      	
If conditions don't fit in one line you should move each condition on a new line with a standard indent.

**Example**:

	bigTerm
	    ? bigVariant
	    : anotherBigTerm
	        ? anotherBigVariant
	        : anotherBigVariant2

#### Promises

When working with promises closures  `[weak self]` isn't used.
<br /> Closures have to be under each other, all end brackets have to be on the same vertical line.

**Example**:

            .then { ...
            	...
            }
            .then { ...
            	...            
            }
            .done { ...
             	...
            }
            .ensure {
            	...
            }
            .catch { 
            	...
            }

#### Arrays

Use `isEmpty` to check to exist for elements in an array.

**Example**:

	array.isEmpty
	
<br /> Referring to elements via `safe`

**Example**:

	array[safe: index]
	
#### Parameters in functions

Each parameter has to be on a separate line if there are a lot of them in a function signature.

**Example**:

    func funcName(
        paramName0: ParamType0
        paramName1: ParamType1,
        paramName2: ParamType2,
        paramName3: ParamType3
	) -> SomeType {

	}
                        	 
The same for function calls.

**Example**:

	router.present(
	    viewController: viewController,
	    usingTransitionContext: context
    )

If there is a trailing closure you must align an end bracket ("}") to parameters.

**Example**:

	self?.router.dismiss(
	    animated: false,
	    completion: {
	        self?.router.pushAuthorizationModule()
        }
    )
   	
#### Constants

Constants must reside in separate enums.

**Example**:

	enum Constants {
	    static let property1: Type1 = value1
	    static let property2: Type2 = value2
	    ...
	}
	
If there are a lot of constants we must group them into different subgroups (by meaning or idea) and each subgroup has to be put in its enum in a scope of a general enum.

**Example**:

	enum Constants {
	    enum Group1 {
	        static let property1: Type1 = value1
	        static let property2: Type2 = value2
	        ...
        }
        
        enum Group2 {
            static let property1: Type1 = value1
            static let property2: Type2 = value2
            ...
        }
    }
    

#### General

* All `IBOutlets` have to be `private`
* Don't add a space between type declaration and its body.

**Example**:

    Wrong:
    class Class {
    
        var property: Type {
            return value
	    }
    }
    
    Correct:
    class Class {
        var property: Type {
            return value
        }
    }
	
* Use `final` modifier if there is no need for a inheritance of a class.
* Explicit strong type in a property declaration: `let value: Int = 0`
* In `.map .contains etc` constructions we use a short form: 
<br /> Instead of `.contains(where: { $0 == .test })` just `.contains { $0 == .test }`
* Unused code must be deleted.

### Restrictions:

* use `FIXME:`
> If there is a need to extract or fix an implementation in the future you have to create a task for that and use `TODO: Will be implemented/fixed in a scope of #TICKET-LINK`.

* use `self` where it's not necessarry
* use `unowned`
> always use `weak`
    
* use `force unwrap`	
